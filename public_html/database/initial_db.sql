/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.28-0ubuntu0.18.04.4 : Database - test_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `api_logs` */

DROP TABLE IF EXISTS `api_logs`;

CREATE TABLE `api_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `params` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `rtime` varchar(255) DEFAULT NULL,
  `authorized` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `general_config` */

DROP TABLE IF EXISTS `general_config`;

CREATE TABLE `general_config` (
  `config_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `config_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `config_value` text CHARACTER SET latin1,
  `deleted` enum('0','1') CHARACTER SET latin1 DEFAULT '0',
  `date_created` bigint(20) DEFAULT NULL,
  `user_created` bigint(20) DEFAULT NULL,
  `date_modified` bigint(20) DEFAULT NULL,
  `user_modified` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `general_config` */

insert  into `general_config`(`config_id`,`rand_id`,`config_name`,`config_value`,`deleted`,`date_created`,`user_created`,`date_modified`,`user_modified`) values 
(1,'email_signature','email_signature','\r\n                                                            \r\n                                                            \r\n                                                            \r\n                                                            <p style=\"color: rgb(34, 34, 34); line-height: 1.6; padding: 0px;\"><b>Best Regards</b></p><div style=\"\"><font color=\"#222222\" size=\"3\"><b>eStudio360.in</b></font><br></div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">Tonk Road, Jaipur</div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">Rajasthan.&nbsp;302018</div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">+91&nbsp;99834 01389</div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">+91&nbsp;88241 42381</div>                                                                                                                                                                                                                                                                                                                                                ','0',1560786104,1,1564722194,1),
(2,'date_format','date_format','M d, Y','0',1560786104,1,1562928584,1),
(3,'logo_white','logo_white','','0',1560786104,1,1565621272,1),
(4,'logo_black','logo_black','','0',1560786104,1,1565621273,1),
(5,'fb_url','fb_url','https://www.facebook.com/VRPatel.m2/','0',1560786104,1,1562944117,1),
(6,'insta_url','insta_url','','0',1560786104,1,1562944117,1),
(7,'linkdin_url','linkdin_url','','0',1560786104,1,1562944117,1),
(8,'youtube_url','youtube_url','','0',1560786104,1,1562944117,1),
(9,'twitter_url','twitter_url','','0',1560786104,1,1562944117,1),
(10,'whatsapp_number','whatsapp_number','+91','0',1560786104,1,1562944117,1),
(11,'pinterest_url','pinterest_url','','0',1560786104,1,1562944117,1),
(12,'tumblr_url','tumblr_url','','0',1560786104,1,1562944118,1),
(13,'map_lang','map_lang','','0',1560786104,1,1564677661,1),
(14,'map_lati','map_lati','','0',1560786104,1,1564677661,1),
(15,'analytics_code','analytics_code',NULL,'0',1560786104,1,NULL,NULL),
(16,'adsense_code','adsense_code',NULL,'0',1560786104,1,NULL,NULL),
(17,'seo_title','seo_title','','0',NULL,NULL,1564677661,1),
(18,'seo_keywords','seo_keywords','','0',NULL,NULL,1564677661,1),
(19,'seo_descriptions','seo_descriptions','','0',NULL,NULL,1564677661,1),
(20,'time_format','time_format','h:i A','0',NULL,NULL,1562928584,1),
(21,'favicon_icon','favicon_icon','','0',NULL,NULL,1565621275,1),
(22,'watermark_type','watermark_type','text','0',NULL,NULL,1565623108,1),
(23,'watermark_large_thumn_text','watermark_large_thumn_text','theFilmyGyan.com','0',NULL,NULL,1565623108,1),
(24,'watermark_large_thumb_font_size','watermark_large_thumb_font_size','30','0',NULL,NULL,1565623108,1),
(25,'watermark_large_thumb_font_color','watermark_large_thumb_font_color','#ffffff','0',NULL,NULL,1565623108,1),
(26,'watermark_large_thumb_vertical_align','watermark_large_thumb_vertical_align','middle','0',NULL,NULL,1565623108,1),
(27,'watermark_large_thumb_horizontal_align','watermark_large_thumb_horizontal_align','right','0',NULL,NULL,1565623109,1),
(28,'watermark_small_thumb_text','watermark_small_thumb_text','theFilmyGyan.com','0',NULL,NULL,1565623109,1),
(29,'watermark_small_thumb_font_size','watermark_small_thumb_font_size','25','0',NULL,NULL,1565623109,1),
(30,'watermark_small_thumb_font_color','watermark_small_thumb_font_color','#ffffff','0',NULL,NULL,1565623109,1),
(31,'watermark_small_thumb_vertical_align','watermark_small_thumb_vertical_align','bottom','0',NULL,NULL,1565623109,1),
(32,'watermark_small_thumb_horizontal_align','watermark_small_thumb_horizontal_align','center','0',NULL,NULL,1565623109,1),
(33,'watermark_in_news','watermark_in_news','1','0',NULL,NULL,1564769431,1),
(34,'watermark_in_news_types','watermark_in_news_types','1','0',NULL,NULL,1565419659,1),
(35,'watermark_in_text_editor','watermark_in_text_editor','1','0',NULL,NULL,1562957678,1),
(36,'send_email_to_new_subscription','send_email_to_new_subscription','1','0',NULL,NULL,NULL,NULL),
(37,'send_a_thanks_email_when_someone_contact_us','send_a_thanks_email_when_someone_contact_us','1','0',NULL,NULL,NULL,NULL),
(38,'watermark_small_thumb_image','watermark_small_thumb_image','','0',NULL,NULL,1565623108,1),
(39,'watermark_large_thumb_image','watermark_large_thumb_image','','0',NULL,NULL,1565623108,1),
(40,'timezone','timezone','Asia/Kolkata','0',NULL,NULL,NULL,NULL);

/*Table structure for table `system_logs` */

DROP TABLE IF EXISTS `system_logs`;

CREATE TABLE `system_logs` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(10) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `log_type` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `timestamp` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` enum('0','Male','Female') DEFAULT '0',
  `oauth_provider` varchar(255) DEFAULT NULL,
  `oauth_id` varchar(255) DEFAULT NULL,
  `user_role_id` bigint(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pin_code` varchar(255) DEFAULT NULL,
  `email_verified` enum('0','1') DEFAULT '0',
  `is_root` enum('0','1') DEFAULT '0',
  `avatar_small` varchar(255) DEFAULT NULL,
  `avatar_medium` varchar(255) DEFAULT NULL,
  `avatar_large` varchar(255) DEFAULT NULL,
  `user_bio` text DEFAULT NULL,
  `language_id` bigint(20) DEFAULT NULL,
  `subscribed_plan_id` bigint(20) DEFAULT NULL,
  `subscription_end_date` date DEFAULT NULL,
  `subscription_payment_id` varchar(255) DEFAULT NULL,
  `email_verification_token` varchar(255) DEFAULT NULL,
  `failed_login_attempt_count` int(2) DEFAULT NULL,
  `terms_accepted` enum('0','1') DEFAULT '0',
  `visibility` enum('0','1') DEFAULT '1',
  `deleted` enum('0','1') DEFAULT '0',
  `date_created` bigint(20) DEFAULT NULL,
  `user_created` bigint(20) DEFAULT NULL,
  `date_modified` bigint(20) DEFAULT NULL,
  `user_modified` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`rand_id`,`first_name`,`last_name`,`email`,`password`,`dob`,`gender`,`oauth_provider`,`oauth_id`,`user_role_id`,`phone`,`address`,`city`,`state`,`pin_code`,`email_verified`,`is_root`,`avatar_small`,`language_id`,`email_verification_token`,`failed_login_attempt_count`,`terms_accepted`,`visibility`,`deleted`,`date_created`,`user_created`,`date_modified`,`user_modified`) values 
(1,'vrpatel','VR','Patel','vrpatel.m@gmail.com','$2y$10$4yWzM6kPrC59zZIZ.plgx.oiAflG1rYAqxm8gKZc7qis/w431zNyO','1997-05-06','Male',NULL,NULL,1,'+91 9983401389',NULL,NULL,NULL,NULL,'1','1',NULL,1,NULL,NULL,'1','1','0',CURRENT_TIMESTAMP,1,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `lang_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) DEFAULT NULL,
  `url_prefix` varchar(10) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `visibility` enum('0','1') DEFAULT '1',
  `deleted` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `languages` */

insert  into `languages`(`lang_id`,`rand_id`,`url_prefix`,`language`,`visibility`,`deleted`) values 
(1,'bangla','bn','Bengali','1','0'),
(2,'english','en','English','1','0'),

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `per_id` bigint NOT NULL AUTO_INCREMENT,
  `permission` varchar(255) NOT NULL,
  `permission_type` enum('Read','Write') NOT NULL DEFAULT 'Read',
  `operation` varchar(255) NOT NULL,
  `role_id` int NOT NULL,
  `module` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'set true if user is display in front site',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_modified` int unsigned DEFAULT NULL,
  `date_created` int unsigned NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint DEFAULT NULL,
  PRIMARY KEY (`per_id`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8 COMMENT='Permissions table';

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'set true if user is display in front site',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_modified` int unsigned DEFAULT NULL,
  `date_created` int unsigned NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8 COMMENT='User roles table';
