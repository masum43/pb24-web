/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 8.0.19-0ubuntu0.19.10.3 : Database - test_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `albums` */

DROP TABLE IF EXISTS `albums`;

CREATE TABLE `albums` (
  `album_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `album_title` varchar(255) NOT NULL,
  `album_image` varchar(255) NOT NULL,
  `album_s_image` varchar(255) NOT NULL,
  `album_l_image` varchar(255) NOT NULL,
  `album_desc` text NOT NULL,
  `public_url` text NOT NULL,
  `album_login_id` varchar(255) NOT NULL,
  `album_pwd` varchar(255) NOT NULL,
  `can_download` enum('0','1') NOT NULL DEFAULT '0',
  `finish_photos_selections` enum('0','1') NOT NULL DEFAULT '0',
  `download_zip_file` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Albums table';

/*Data for the table `albums` */

/*Table structure for table `albums_users_trans` */

DROP TABLE IF EXISTS `albums_users_trans`;

CREATE TABLE `albums_users_trans` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `album_id` bigint NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Albums users trans table';

/*Data for the table `albums_users_trans` */

/*Table structure for table `api_logs` */

DROP TABLE IF EXISTS `api_logs`;

CREATE TABLE `api_logs` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `params` varchar(255) DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `rtime` varchar(255) DEFAULT NULL,
  `authorized` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

/*Data for the table `api_logs` */

insert  into `api_logs`(`id`,`uri`,`method`,`params`,`ip_address`,`api_key`,`time`,`rtime`,`authorized`) values 
(1,'en/api/users/user_login','post','','127.0.0.1','','1579951496','0.13021302223206','1'),
(2,'en/api/users/user_login','post','','127.0.0.1','','1579951601','1.0332617759705','1'),
(3,'en/api/users/user_login','post','','127.0.0.1','','1579951611','0.36920213699341','1'),
(4,'en/api/users/user_login','post','','127.0.0.1','','1579951661','0.38005089759827','1'),
(5,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579951708','0.1640191078186','1'),
(6,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579951743','0.23636913299561','1'),
(7,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:0:\"\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579952183','0.068128108978271','1'),
(8,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:0:\"\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579952187','0.036679029464722','1'),
(9,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:0:\"\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579952190','0.046905040740967','1'),
(10,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:0:\"\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579952193','0.94044709205627','1'),
(11,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579952206','0.36023783683777','1'),
(12,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953256','0.17725491523743','1'),
(13,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:3:\"ssd\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953264','0.039085865020752','1'),
(14,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:20:\"admin@admin.comasdad\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953270','0.24608778953552','1'),
(15,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:20:\"admin@admin.comasdad\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953347','0.17717289924622','1'),
(16,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953356','0.35149908065796','1'),
(17,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953361','0.22894716262817','1'),
(18,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953438','0.16308808326721','1'),
(19,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953443','0.16483616828918','1'),
(20,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:0:\"\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953843','0.03397798538208','1'),
(21,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:5:\"ad,om\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953847','0.054286003112793','1'),
(22,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953853','0.33745098114014','1'),
(23,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953856','0.17890787124634','1'),
(24,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579953929','0.15706706047058','1'),
(25,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579954006','0.14492702484131','1'),
(26,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:0:\"\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579956452','0.041326999664307','1'),
(27,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579956460','0.24386405944824','1'),
(28,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:0:\"\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579957296','0.036377906799316','1'),
(29,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579957303','0.24095582962036','1'),
(30,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1579958103','0.25374412536621','1'),
(31,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580031199','0.46333503723145','1'),
(32,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580037939','0.25858807563782','1'),
(33,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580141368','0.28472185134888','1'),
(34,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580142957','0.27161192893982','1'),
(35,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580763068','0.36942911148071','1'),
(36,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580763072','0.50551986694336','1'),
(37,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580763077','0.43912100791931','1'),
(38,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580745204','0.63649296760559','1'),
(39,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1580749375','0.34055113792419','1'),
(40,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1581164573','0.59039211273193','1'),
(41,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1581164917','0.2471559047699','1'),
(42,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:5:\"admin\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1581894366','0.15883994102478','1'),
(43,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1581894382','0.19082188606262','1'),
(44,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1581942460','0.32513403892517','1'),
(45,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1582308419','0.4872739315033','1'),
(46,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1582358038','0.33655285835266','1'),
(47,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1582358050','0.52645492553711','1'),
(48,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1582389778','0.19508385658264','1'),
(49,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:18:\"jhgadmin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1583319463','0.3530478477478','1'),
(50,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1583319467','0.4618661403656','1'),
(51,'en/api/users/user_login','post','a:2:{s:5:\"email\";s:15:\"admin@admin.com\";s:8:\"password\";s:10:\"xxxxxxxxxx\";}','127.0.0.1','','1583653642','1.3089299201965','1');

/*Table structure for table `bookings` */

DROP TABLE IF EXISTS `bookings`;

CREATE TABLE `bookings` (
  `book_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `mobile_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `evt_type_id` bigint NOT NULL,
  `package_id` bigint NOT NULL,
  `descriptions` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `read` enum('0','1') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint DEFAULT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bookings table';

/*Data for the table `bookings` */

/*Table structure for table `contact_supports` */

DROP TABLE IF EXISTS `contact_supports`;

CREATE TABLE `contact_supports` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `support_type` varchar(255) NOT NULL,
  `client` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` enum('Unread','Read','Resolved') NOT NULL DEFAULT 'Unread',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contact supports table';

/*Data for the table `contact_supports` */

/*Table structure for table `event_types` */

DROP TABLE IF EXISTS `event_types`;

CREATE TABLE `event_types` (
  `evt_typ_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `evt_type` varchar(255) NOT NULL,
  `evt_type_image` varchar(255) NOT NULL,
  `evt_type_s_image` varchar(255) NOT NULL,
  `evt_type_l_image` varchar(255) NOT NULL,
  `evt_type_desc` text NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_desc` text NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`evt_typ_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Event types table';

/*Data for the table `event_types` */

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `evt_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `evt_title` varchar(255) NOT NULL,
  `evt_image` varchar(255) NOT NULL,
  `evt_s_image` varchar(255) NOT NULL,
  `evt_l_image` varchar(255) NOT NULL,
  `evt_desc` text NOT NULL,
  `evt_type_id` bigint NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `can_download` enum('0','1') NOT NULL DEFAULT '0',
  `seo_title` varchar(255) NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_desc` text NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`evt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Events table';

/*Data for the table `events` */

/*Table structure for table `failed_images` */

DROP TABLE IF EXISTS `failed_images`;

CREATE TABLE `failed_images` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `img_full_path` varchar(255) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `scope` varchar(255) NOT NULL,
  `operation` varchar(255) NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Failed images table';

/*Data for the table `failed_images` */

/*Table structure for table `faq` */

DROP TABLE IF EXISTS `faq`;

CREATE TABLE `faq` (
  `faq_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `faq_title` varchar(255) NOT NULL,
  `faq_desc` text NOT NULL,
  `faq_icon` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`faq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='FAQ table';

/*Data for the table `faq` */

/*Table structure for table `general_config` */

DROP TABLE IF EXISTS `general_config`;

CREATE TABLE `general_config` (
  `config_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `config_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `config_value` text CHARACTER SET latin1,
  `deleted` enum('0','1') CHARACTER SET latin1 DEFAULT '0',
  `date_created` bigint DEFAULT NULL,
  `user_created` bigint DEFAULT NULL,
  `date_modified` bigint DEFAULT NULL,
  `user_modified` bigint DEFAULT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `general_config` */

insert  into `general_config`(`config_id`,`rand_id`,`config_name`,`config_value`,`deleted`,`date_created`,`user_created`,`date_modified`,`user_modified`) values 
(1,'timezone','timezone','Asia/Kolkata','0',NULL,NULL,NULL,NULL);

/*Table structure for table `home_gallery` */

DROP TABLE IF EXISTS `home_gallery`;

CREATE TABLE `home_gallery` (
  `img_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `img_title` varchar(255) NOT NULL,
  `img_alt` varchar(255) NOT NULL,
  `img_href` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Home Gallery table';

/*Data for the table `home_gallery` */

/*Table structure for table `home_slider` */

DROP TABLE IF EXISTS `home_slider`;

CREATE TABLE `home_slider` (
  `img_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `img_title` varchar(255) NOT NULL,
  `img_alt` varchar(255) NOT NULL,
  `img_href` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Home Slider table';

/*Data for the table `home_slider` */

/*Table structure for table `images_gallery` */

DROP TABLE IF EXISTS `images_gallery`;

CREATE TABLE `images_gallery` (
  `img_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `img_code` varchar(255) NOT NULL,
  `img_old_name` varchar(255) NOT NULL,
  `img_new_name` varchar(255) NOT NULL,
  `small_thumb` varchar(255) NOT NULL,
  `large_thumb` varchar(255) NOT NULL,
  `img_title` varchar(255) NOT NULL,
  `img_alt` varchar(255) NOT NULL,
  `entity` varchar(255) NOT NULL,
  `entity_id` bigint NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_desc` text NOT NULL,
  `status` enum('0','Approved','Rejected') NOT NULL DEFAULT '0',
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Images gallery table';

/*Data for the table `images_gallery` */

/*Table structure for table `languages` */

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `lang_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) DEFAULT NULL,
  `url_prefix` varchar(10) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `visibility` enum('0','1') DEFAULT '1',
  `deleted` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `languages` */

insert  into `languages`(`lang_id`,`rand_id`,`url_prefix`,`language`,`visibility`,`deleted`) values 
(1,'hindi','hi','Hindi','1','0'),
(2,'english','en','English','1','0'),
(3,'punjabi','pb','Punjabi','1','1'),
(4,'gujarati','gu','Gujarati','1','1');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `migrations` */

insert  into `migrations`(`version`) values 
(20190620184945);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `noti_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `noti_title` varchar(255) NOT NULL,
  `noti_icon` varchar(255) NOT NULL,
  `noti_href` varchar(255) NOT NULL,
  `noti_desc` text NOT NULL,
  `client_desc` varchar(255) NOT NULL,
  `client_image` varchar(255) NOT NULL,
  `read` enum('0','1') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`noti_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Notifications table';

/*Data for the table `notifications` */

/*Table structure for table `notifications_users_trans` */

DROP TABLE IF EXISTS `notifications_users_trans`;

CREATE TABLE `notifications_users_trans` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NOT NULL,
  `notification_id` bigint NOT NULL,
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Notifications users trans table';

/*Data for the table `notifications_users_trans` */

/*Table structure for table `our_services` */

DROP TABLE IF EXISTS `our_services`;

CREATE TABLE `our_services` (
  `ser_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `ser_img` varchar(255) NOT NULL,
  `ser_s_img` varchar(255) NOT NULL,
  `ser_l_img` varchar(255) NOT NULL,
  `ser_title` varchar(255) NOT NULL,
  `s_desc` varchar(255) NOT NULL,
  `l_desc` text NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_desc` text NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`ser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Our Services table';

/*Data for the table `our_services` */

/*Table structure for table `package_types` */

DROP TABLE IF EXISTS `package_types`;

CREATE TABLE `package_types` (
  `pktyp_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`pktyp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Package types table';

/*Data for the table `package_types` */

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `pack_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `type_id` bigint NOT NULL,
  `descriptions` text NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`pack_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Packages table';

/*Data for the table `packages` */

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `per_id` bigint NOT NULL AUTO_INCREMENT,
  `permission` varchar(255) NOT NULL,
  `permission_type` enum('Read','Write') NOT NULL DEFAULT 'Read',
  `operation` varchar(255) NOT NULL,
  `role_id` int NOT NULL,
  `module` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1' COMMENT 'set true if user is display in front site',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_modified` int unsigned DEFAULT NULL,
  `date_created` int unsigned NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint DEFAULT NULL,
  PRIMARY KEY (`per_id`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8 COMMENT='Permissions table';

/*Data for the table `permissions` */

insert  into `permissions`(`per_id`,`permission`,`permission_type`,`operation`,`role_id`,`module`,`action`,`visibility`,`deleted`,`date_modified`,`date_created`,`user_created`,`user_modified`) values 
(1,'1','Read','view_blogs_listing_page',1,'blogs','index','1','0',NULL,1580749288,1,NULL),
(2,'1','Read','view_blogs_listing_page',2,'blogs','index','1','0',NULL,1580749288,1,NULL),
(3,'0','Read','view_blogs_listing_page',3,'blogs','index','1','0',NULL,1580749288,1,NULL),
(4,'1','Read','add_new_blog',1,'blogs','add','1','0',NULL,1580749288,1,NULL),
(5,'1','Read','add_new_blog',2,'blogs','add','1','0',NULL,1580749288,1,NULL),
(6,'0','Read','add_new_blog',3,'blogs','add','1','0',NULL,1580749288,1,NULL),
(7,'1','Read','view_blog_page',1,'blogs','view','1','0',NULL,1580749288,1,NULL),
(8,'1','Read','view_blog_page',2,'blogs','view','1','0',NULL,1580749288,1,NULL),
(9,'0','Read','view_blog_page',3,'blogs','view','1','0',NULL,1580749288,1,NULL),
(10,'1','Read','update_blog_details',1,'blogs','edit','1','0',NULL,1580749288,1,NULL),
(11,'1','Read','update_blog_details',2,'blogs','edit','1','0',NULL,1580749288,1,NULL),
(12,'0','Read','update_blog_details',3,'blogs','edit','1','0',NULL,1580749288,1,NULL),
(13,'1','Read','delete_blog_details',1,'blogs','delete','1','0',NULL,1580749288,1,NULL),
(14,'1','Read','delete_blog_details',2,'blogs','delete','1','0',NULL,1580749288,1,NULL),
(15,'0','Read','delete_blog_details',3,'blogs','delete','1','0',NULL,1580749288,1,NULL),
(16,'1','Read','view_reminders_listing_page',1,'reminders','index','1','0',NULL,1580749288,1,NULL),
(17,'1','Read','view_reminders_listing_page',2,'reminders','index','1','0',NULL,1580749288,1,NULL),
(18,'0','Read','view_reminders_listing_page',3,'reminders','index','1','0',NULL,1580749288,1,NULL),
(19,'1','Read','add_new_reminder',1,'reminders','add','1','0',NULL,1580749288,1,NULL),
(20,'1','Read','add_new_reminder',2,'reminders','add','1','0',NULL,1580749288,1,NULL),
(21,'0','Read','add_new_reminder',3,'reminders','add','1','0',NULL,1580749288,1,NULL),
(22,'1','Read','view_reminder_details_page',1,'reminders','view','1','0',NULL,1580749288,1,NULL),
(23,'1','Read','view_reminder_details_page',2,'reminders','view','1','0',NULL,1580749288,1,NULL),
(24,'0','Read','view_reminder_details_page',3,'reminders','view','1','0',NULL,1580749288,1,NULL),
(25,'1','Read','update_reminder_details',1,'reminders','edit','1','0',NULL,1580749288,1,NULL),
(26,'1','Read','update_reminder_details',2,'reminders','edit','1','0',NULL,1580749288,1,NULL),
(27,'0','Read','update_reminder_details',3,'reminders','edit','1','0',NULL,1580749288,1,NULL),
(28,'1','Read','delete_reminder_details',1,'reminders','delete','1','0',NULL,1580749288,1,NULL),
(29,'1','Read','delete_reminder_details',2,'reminders','delete','1','0',NULL,1580749288,1,NULL),
(30,'0','Read','delete_reminder_details',3,'reminders','delete','1','0',NULL,1580749288,1,NULL),
(31,'1','Read','view_albums_listing_page',1,'albums','index','1','0',NULL,1580749288,1,NULL),
(32,'1','Read','view_albums_listing_page',2,'albums','index','1','0',NULL,1580749288,1,NULL),
(33,'1','Read','view_albums_listing_page',3,'albums','index','1','0',NULL,1580749288,1,NULL),
(34,'1','Read','add_new_album',1,'albums','add','1','0',NULL,1580749288,1,NULL),
(35,'1','Read','add_new_album',2,'albums','add','1','0',NULL,1580749288,1,NULL),
(36,'0','Read','add_new_album',3,'albums','add','1','0',NULL,1580749288,1,NULL),
(37,'1','Read','view_album_details_page',1,'albums','view','1','0',NULL,1580749288,1,NULL),
(38,'1','Read','view_album_details_page',2,'albums','view','1','0',NULL,1580749288,1,NULL),
(39,'1','Read','view_album_details_page',3,'albums','view','1','0',NULL,1580749288,1,NULL),
(40,'1','Read','update_album_details',1,'albums','edit','1','0',NULL,1580749288,1,NULL),
(41,'1','Read','update_album_details',2,'albums','edit','1','0',NULL,1580749288,1,NULL),
(42,'0','Read','update_album_details',3,'albums','edit','1','0',NULL,1580749288,1,NULL),
(43,'1','Read','delete_album_details',1,'albums','delete','1','0',NULL,1580749288,1,NULL),
(44,'1','Read','delete_album_details',2,'albums','delete','1','0',NULL,1580749288,1,NULL),
(45,'0','Read','delete_album_details',3,'albums','delete','1','0',NULL,1580749288,1,NULL),
(46,'1','Read','view_live_streamings_listing_page',1,'live_streamings','index','1','0',NULL,1580749288,1,NULL),
(47,'1','Read','view_live_streamings_listing_page',2,'live_streamings','index','1','0',NULL,1580749288,1,NULL),
(48,'0','Read','view_live_streamings_listing_page',3,'live_streamings','index','1','0',NULL,1580749288,1,NULL),
(49,'1','Read','add_new_live_streaming',1,'live_streamings','add','1','0',NULL,1580749288,1,NULL),
(50,'1','Read','add_new_live_streaming',2,'live_streamings','add','1','0',NULL,1580749288,1,NULL),
(51,'0','Read','add_new_live_streaming',3,'live_streamings','add','1','0',NULL,1580749288,1,NULL),
(52,'1','Read','view_live_streaming_details_page',1,'live_streamings','view','1','0',NULL,1580749288,1,NULL),
(53,'1','Read','view_live_streaming_details_page',2,'live_streamings','view','1','0',NULL,1580749288,1,NULL),
(54,'0','Read','view_live_streaming_details_page',3,'live_streamings','view','1','0',NULL,1580749288,1,NULL),
(55,'1','Read','update_live_streaming_details',1,'live_streamings','edit','1','0',NULL,1580749288,1,NULL),
(56,'1','Read','update_live_streaming_details',2,'live_streamings','edit','1','0',NULL,1580749288,1,NULL),
(57,'0','Read','update_live_streaming_details',3,'live_streamings','edit','1','0',NULL,1580749288,1,NULL),
(58,'1','Read','delete_live_streaming_details',1,'live_streamings','delete','1','0',NULL,1580749288,1,NULL),
(59,'1','Read','delete_live_streaming_details',2,'live_streamings','delete','1','0',NULL,1580749288,1,NULL),
(60,'0','Read','delete_live_streaming_details',3,'live_streamings','delete','1','0',NULL,1580749288,1,NULL),
(61,'1','Read','view_videos_listing_page',1,'videos','index','1','0',NULL,1580749288,1,NULL),
(62,'1','Read','view_videos_listing_page',2,'videos','index','1','0',NULL,1580749288,1,NULL),
(63,'0','Read','view_videos_listing_page',3,'videos','index','1','0',NULL,1580749288,1,NULL),
(64,'1','Read','add_new_video',1,'videos','add','1','0',NULL,1580749288,1,NULL),
(65,'1','Read','add_new_video',2,'videos','add','1','0',NULL,1580749288,1,NULL),
(66,'0','Read','add_new_video',3,'videos','add','1','0',NULL,1580749288,1,NULL),
(67,'1','Read','view_video_details_page',1,'videos','view','1','0',NULL,1580749288,1,NULL),
(68,'1','Read','view_video_details_page',2,'videos','view','1','0',NULL,1580749288,1,NULL),
(69,'0','Read','view_video_details_page',3,'videos','view','1','0',NULL,1580749288,1,NULL),
(70,'1','Read','update_video_details',1,'videos','edit','1','0',NULL,1580749288,1,NULL),
(71,'1','Read','update_video_details',2,'videos','edit','1','0',NULL,1580749288,1,NULL),
(72,'0','Read','update_video_details',3,'videos','edit','1','0',NULL,1580749288,1,NULL),
(73,'1','Read','delete_video_details',1,'videos','delete','1','0',NULL,1580749288,1,NULL),
(74,'1','Read','delete_video_details',2,'videos','delete','1','0',NULL,1580749288,1,NULL),
(75,'0','Read','delete_video_details',3,'videos','delete','1','0',NULL,1580749288,1,NULL),
(76,'1','Read','view_events_listing_page',1,'events','index','1','0',NULL,1580749288,1,NULL),
(77,'1','Read','view_events_listing_page',2,'events','index','1','0',NULL,1580749288,1,NULL),
(78,'0','Read','view_events_listing_page',3,'events','index','1','0',NULL,1580749288,1,NULL),
(79,'1','Read','add_new_event',1,'events','add','1','0',NULL,1580749288,1,NULL),
(80,'1','Read','add_new_event',2,'events','add','1','0',NULL,1580749288,1,NULL),
(81,'0','Read','add_new_event',3,'events','add','1','0',NULL,1580749288,1,NULL),
(82,'1','Read','view_event_details_page',1,'events','view','1','0',NULL,1580749288,1,NULL),
(83,'1','Read','view_event_details_page',2,'events','view','1','0',NULL,1580749288,1,NULL),
(84,'0','Read','view_event_details_page',3,'events','view','1','0',NULL,1580749288,1,NULL),
(85,'1','Read','update_event_details',1,'events','edit','1','0',NULL,1580749288,1,NULL),
(86,'1','Read','update_event_details',2,'events','edit','1','0',NULL,1580749288,1,NULL),
(87,'0','Read','update_event_details',3,'events','edit','1','0',NULL,1580749288,1,NULL),
(88,'1','Read','delete_event_details',1,'events','delete','1','0',NULL,1580749288,1,NULL),
(89,'1','Read','delete_event_details',2,'events','delete','1','0',NULL,1580749288,1,NULL),
(90,'0','Read','delete_event_details',3,'events','delete','1','0',NULL,1580749288,1,NULL),
(91,'1','Read','view_event_types_listing_page',1,'event_types','index','1','0',NULL,1580749288,1,NULL),
(92,'1','Read','view_event_types_listing_page',2,'event_types','index','1','0',NULL,1580749288,1,NULL),
(93,'0','Read','view_event_types_listing_page',3,'event_types','index','1','0',NULL,1580749288,1,NULL),
(94,'1','Read','add_new_event_type',1,'event_types','add','1','0',NULL,1580749288,1,NULL),
(95,'1','Read','add_new_event_type',2,'event_types','add','1','0',NULL,1580749288,1,NULL),
(96,'0','Read','add_new_event_type',3,'event_types','add','1','0',NULL,1580749288,1,NULL),
(97,'1','Read','view_event_type_details_page',1,'event_types','view','1','0',NULL,1580749288,1,NULL),
(98,'1','Read','view_event_type_details_page',2,'event_types','view','1','0',NULL,1580749288,1,NULL),
(99,'0','Read','view_event_type_details_page',3,'event_types','view','1','0',NULL,1580749288,1,NULL),
(100,'1','Read','update_event_type_details',1,'event_types','edit','1','0',NULL,1580749288,1,NULL),
(101,'1','Read','update_event_type_details',2,'event_types','edit','1','0',NULL,1580749288,1,NULL),
(102,'0','Read','update_event_type_details',3,'event_types','edit','1','0',NULL,1580749288,1,NULL),
(103,'1','Read','delete_event_type_details',1,'event_types','delete','1','0',NULL,1580749288,1,NULL),
(104,'1','Read','delete_event_type_details',2,'event_types','delete','1','0',NULL,1580749288,1,NULL),
(105,'0','Read','delete_event_type_details',3,'event_types','delete','1','0',NULL,1580749288,1,NULL),
(106,'1','Read','view_about_us_section',1,'about_us','index','1','0',NULL,1580749288,1,NULL),
(107,'1','Read','view_about_us_section',2,'about_us','index','1','0',NULL,1580749288,1,NULL),
(108,'0','Read','view_about_us_section',3,'about_us','index','1','0',NULL,1580749288,1,NULL),
(109,'1','Read','edit_about_us_section',1,'about_us','edit','1','0',NULL,1580749288,1,NULL),
(110,'1','Read','edit_about_us_section',2,'about_us','edit','1','0',NULL,1580749288,1,NULL),
(111,'0','Read','edit_about_us_section',3,'about_us','edit','1','0',NULL,1580749288,1,NULL),
(112,'1','Read','view_privacy_policy_section',1,'privacy_policy','index','1','0',NULL,1580749288,1,NULL),
(113,'1','Read','view_privacy_policy_section',2,'privacy_policy','index','1','0',NULL,1580749288,1,NULL),
(114,'0','Read','view_privacy_policy_section',3,'privacy_policy','index','1','0',NULL,1580749288,1,NULL),
(115,'1','Read','edit_privacy_policy_section',1,'privacy_policy','edit','1','0',NULL,1580749288,1,NULL),
(116,'1','Read','edit_privacy_policy_section',2,'privacy_policy','edit','1','0',NULL,1580749288,1,NULL),
(117,'0','Read','edit_privacy_policy_section',3,'privacy_policy','edit','1','0',NULL,1580749288,1,NULL),
(118,'1','Read','view_terms_and_conditions_section',1,'terms_and_conditions','index','1','0',NULL,1580749288,1,NULL),
(119,'1','Read','view_terms_and_conditions_section',2,'terms_and_conditions','index','1','0',NULL,1580749288,1,NULL),
(120,'0','Read','view_terms_and_conditions_section',3,'terms_and_conditions','index','1','0',NULL,1580749288,1,NULL),
(121,'1','Read','edit_terms_and_conditions_section',1,'terms_and_conditions','edit','1','0',NULL,1580749288,1,NULL),
(122,'1','Read','edit_terms_and_conditions_section',2,'terms_and_conditions','edit','1','0',NULL,1580749288,1,NULL),
(123,'0','Read','edit_terms_and_conditions_section',3,'terms_and_conditions','edit','1','0',NULL,1580749288,1,NULL),
(124,'1','Read','view_home_slider_page',1,'home_slider','index','1','0',NULL,1580749288,1,NULL),
(125,'1','Read','view_home_slider_page',2,'home_slider','index','1','0',NULL,1580749288,1,NULL),
(126,'0','Read','view_home_slider_page',3,'home_slider','index','1','0',NULL,1580749288,1,NULL),
(127,'1','Read','upload_photo_in_home_slider_page',1,'home_slider','upload','1','0',NULL,1580749288,1,NULL),
(128,'1','Read','upload_photo_in_home_slider_page',2,'home_slider','upload','1','0',NULL,1580749288,1,NULL),
(129,'0','Read','upload_photo_in_home_slider_page',3,'home_slider','upload','1','0',NULL,1580749288,1,NULL),
(130,'1','Read','delete_photo_in_home_slider_page',1,'home_slider','delete','1','0',NULL,1580749288,1,NULL),
(131,'1','Read','delete_photo_in_home_slider_page',2,'home_slider','delete','1','0',NULL,1580749288,1,NULL),
(132,'0','Read','delete_photo_in_home_slider_page',3,'home_slider','delete','1','0',NULL,1580749288,1,NULL),
(133,'1','Read','view_home_gallery_page',1,'home_gallery','index','1','0',NULL,1580749288,1,NULL),
(134,'1','Read','view_home_gallery_page',2,'home_gallery','index','1','0',NULL,1580749288,1,NULL),
(135,'0','Read','view_home_gallery_page',3,'home_gallery','index','1','0',NULL,1580749288,1,NULL),
(136,'1','Read','upload_photo_in_home_gallery_page',1,'home_gallery','upload','1','0',NULL,1580749288,1,NULL),
(137,'1','Read','upload_photo_in_home_gallery_page',2,'home_gallery','upload','1','0',NULL,1580749288,1,NULL),
(138,'0','Read','upload_photo_in_home_gallery_page',3,'home_gallery','upload','1','0',NULL,1580749288,1,NULL),
(139,'1','Read','delete_photo_in_home_gallery_page',1,'home_gallery','delete','1','0',NULL,1580749288,1,NULL),
(140,'1','Read','delete_photo_in_home_gallery_page',2,'home_gallery','delete','1','0',NULL,1580749288,1,NULL),
(141,'0','Read','delete_photo_in_home_gallery_page',3,'home_gallery','delete','1','0',NULL,1580749288,1,NULL),
(142,'1','Read','view_tags_listing_page',1,'tags','index','1','0',NULL,1580749288,1,NULL),
(143,'1','Read','view_tags_listing_page',2,'tags','index','1','0',NULL,1580749288,1,NULL),
(144,'0','Read','view_tags_listing_page',3,'tags','index','1','0',NULL,1580749288,1,NULL),
(145,'1','Read','add_new_tag',1,'tags','add','1','0',NULL,1580749288,1,NULL),
(146,'1','Read','add_new_tag',2,'tags','add','1','0',NULL,1580749288,1,NULL),
(147,'0','Read','add_new_tag',3,'tags','add','1','0',NULL,1580749288,1,NULL),
(148,'1','Read','view_tag_details_page',1,'tags','view','1','0',NULL,1580749288,1,NULL),
(149,'1','Read','view_tag_details_page',2,'tags','view','1','0',NULL,1580749288,1,NULL),
(150,'0','Read','view_tag_details_page',3,'tags','view','1','0',NULL,1580749288,1,NULL),
(151,'1','Read','update_tag_details',1,'tags','edit','1','0',NULL,1580749288,1,NULL),
(152,'1','Read','update_tag_details',2,'tags','edit','1','0',NULL,1580749288,1,NULL),
(153,'0','Read','update_tag_details',3,'tags','edit','1','0',NULL,1580749288,1,NULL),
(154,'1','Read','delete_tag_details',1,'tags','delete','1','0',NULL,1580749288,1,NULL),
(155,'1','Read','delete_tag_details',2,'tags','delete','1','0',NULL,1580749288,1,NULL),
(156,'0','Read','delete_tag_details',3,'tags','delete','1','0',NULL,1580749288,1,NULL),
(157,'1','Read','view_testimonials_listing_page',1,'testimonial','index','1','0',NULL,1580749288,1,NULL),
(158,'1','Read','view_testimonials_listing_page',2,'testimonial','index','1','0',NULL,1580749288,1,NULL),
(159,'0','Read','view_testimonials_listing_page',3,'testimonial','index','1','0',NULL,1580749288,1,NULL),
(160,'1','Read','add_new_testimonial',1,'testimonial','add','1','0',NULL,1580749288,1,NULL),
(161,'1','Read','add_new_testimonial',2,'testimonial','add','1','0',NULL,1580749288,1,NULL),
(162,'0','Read','add_new_testimonial',3,'testimonial','add','1','0',NULL,1580749288,1,NULL),
(163,'1','Read','view_testimonial_details_page',1,'testimonial','view','1','0',NULL,1580749288,1,NULL),
(164,'1','Read','view_testimonial_details_page',2,'testimonial','view','1','0',NULL,1580749288,1,NULL),
(165,'0','Read','view_testimonial_details_page',3,'testimonial','view','1','0',NULL,1580749288,1,NULL),
(166,'1','Read','update_testimonial_details',1,'testimonial','edit','1','0',NULL,1580749288,1,NULL),
(167,'1','Read','update_testimonial_details',2,'testimonial','edit','1','0',NULL,1580749288,1,NULL),
(168,'0','Read','update_testimonial_details',3,'testimonial','edit','1','0',NULL,1580749288,1,NULL),
(169,'1','Read','delete_testimonial_details',1,'testimonial','delete','1','0',NULL,1580749288,1,NULL),
(170,'1','Read','delete_testimonial_details',2,'testimonial','delete','1','0',NULL,1580749288,1,NULL),
(171,'0','Read','delete_testimonial_details',3,'testimonial','delete','1','0',NULL,1580749288,1,NULL),
(172,'1','Read','view_our_services_listing_page',1,'our_services','index','1','0',NULL,1580749288,1,NULL),
(173,'1','Read','view_our_services_listing_page',2,'our_services','index','1','0',NULL,1580749288,1,NULL),
(174,'0','Read','view_our_services_listing_page',3,'our_services','index','1','0',NULL,1580749288,1,NULL),
(175,'1','Read','add_new_our_service',1,'our_services','add','1','0',NULL,1580749288,1,NULL),
(176,'1','Read','add_new_our_service',2,'our_services','add','1','0',NULL,1580749288,1,NULL),
(177,'0','Read','add_new_our_service',3,'our_services','add','1','0',NULL,1580749288,1,NULL),
(178,'1','Read','view_our_service_details_page',1,'our_services','view','1','0',NULL,1580749288,1,NULL),
(179,'1','Read','view_our_service_details_page',2,'our_services','view','1','0',NULL,1580749288,1,NULL),
(180,'0','Read','view_our_service_details_page',3,'our_services','view','1','0',NULL,1580749288,1,NULL),
(181,'1','Read','update_our_service_details',1,'our_services','edit','1','0',NULL,1580749288,1,NULL),
(182,'1','Read','update_our_service_details',2,'our_services','edit','1','0',NULL,1580749288,1,NULL),
(183,'0','Read','update_our_service_details',3,'our_services','edit','1','0',NULL,1580749288,1,NULL),
(184,'1','Read','delete_our_service_details',1,'our_services','delete','1','0',NULL,1580749288,1,NULL),
(185,'1','Read','delete_our_service_details',2,'our_services','delete','1','0',NULL,1580749288,1,NULL),
(186,'0','Read','delete_our_service_details',3,'our_services','delete','1','0',NULL,1580749288,1,NULL),
(187,'1','Read','upload_photos_in_album',1,'albums','upload','1','0',NULL,1580749288,1,NULL),
(188,'1','Read','upload_photos_in_album',2,'albums','upload','1','0',NULL,1580749288,1,NULL),
(189,'0','Read','upload_photos_in_album',3,'albums','upload','1','0',NULL,1580749288,1,NULL),
(190,'1','Read','delete_photo_from_album',1,'albums','delete_image','1','0',NULL,1580749288,1,NULL),
(191,'1','Read','delete_photo_from_album',2,'albums','delete_image','1','0',NULL,1580749288,1,NULL),
(192,'0','Read','delete_photo_from_album',3,'albums','delete_image','1','0',NULL,1580749288,1,NULL),
(193,'1','Read','upload_photos_in_an_event',1,'events','upload','1','0',NULL,1580749288,1,NULL),
(194,'1','Read','upload_photos_in_an_event',2,'events','upload','1','0',NULL,1580749288,1,NULL),
(195,'0','Read','upload_photos_in_an_event',3,'events','upload','1','0',NULL,1580749288,1,NULL),
(196,'1','Read','delete_photo_from_an_event',1,'events','delete_image','1','0',NULL,1580749288,1,NULL),
(197,'1','Read','delete_photo_from_an_event',2,'events','delete_image','1','0',NULL,1580749288,1,NULL),
(198,'0','Read','delete_photo_from_an_event',3,'events','delete_image','1','0',NULL,1580749288,1,NULL),
(199,'1','Read','view_settings_page',1,'settings','index','1','0',NULL,1580749288,1,NULL),
(200,'1','Read','view_users_page',1,'users','index','1','0',NULL,1580749288,1,NULL),
(201,'1','Read','view_users_page',2,'users','index','1','0',NULL,1580749288,1,NULL),
(202,'0','Read','view_users_page',3,'users','index','1','0',NULL,1580749288,1,NULL),
(203,'1','Read','add_new_user',1,'users','add','1','0',NULL,1580749288,1,NULL),
(204,'1','Read','add_new_user',2,'users','add','1','0',NULL,1580749288,1,NULL),
(205,'0','Read','add_new_user',3,'users','add','1','0',NULL,1580749288,1,NULL),
(206,'1','Read','edit_user_details',1,'users','edit','1','0',NULL,1580749288,1,NULL),
(207,'1','Read','edit_user_details',2,'users','edit','1','0',NULL,1580749288,1,NULL),
(208,'0','Read','edit_user_details',3,'users','edit','1','0',NULL,1580749288,1,NULL),
(209,'1','Read','delete_user_details',1,'users','delete','1','0',NULL,1580749288,1,NULL),
(210,'1','Read','delete_user_details',2,'users','delete','1','0',NULL,1580749288,1,NULL),
(211,'0','Read','delete_user_details',3,'users','delete','1','0',NULL,1580749288,1,NULL),
(212,'1','Read','view_user_details',1,'users','view','1','0',NULL,1580749288,1,NULL),
(213,'1','Read','view_user_details',2,'users','view','1','0',NULL,1580749288,1,NULL),
(214,'0','Read','view_user_details',3,'users','view','1','0',NULL,1580749288,1,NULL),
(215,'1','Read','view_permissions_page',1,'permissions','index','1','0',NULL,1580749288,1,NULL),
(216,'0','Read','view_permissions_page',2,'permissions','index','1','0',NULL,1580749288,1,NULL),
(217,'0','Read','view_permissions_page',3,'permissions','index','1','0',NULL,1580749288,1,NULL),
(218,'1','Read','view_system_logs_page',1,'system_logs','index','1','0',NULL,1580749288,1,NULL),
(219,'0','Read','view_system_logs_page',2,'system_logs','index','1','0',NULL,1580749288,1,NULL),
(220,'0','Read','view_system_logs_page',3,'system_logs','index','1','0',NULL,1580749288,1,NULL),
(221,'1','Read','view_settings_page',2,'settings','index','1','0',NULL,1580749288,1,NULL),
(222,'0','Read','view_settings_page',3,'settings','index','1','0',NULL,1580749288,1,NULL),
(223,'1','Read','view_studio_details_page',1,'client_details','index','1','0',NULL,1580749288,1,NULL),
(224,'1','Read','view_studio_details_page',2,'client_details','index','1','0',NULL,1580749288,1,NULL),
(225,'0','Read','view_studio_details_page',3,'client_details','index','1','0',NULL,1580749288,1,NULL),
(226,'1','Read','edit_studio_details',1,'client_details','edit','1','0',NULL,1580749288,1,NULL),
(227,'1','Read','edit_studio_details',2,'client_details','edit','1','0',NULL,1580749288,1,NULL),
(228,'0','Read','edit_studio_details',3,'client_details','edit','1','0',NULL,1580749288,1,NULL),
(229,'1','Read','view_email_signature_page',1,'email_signature','index','1','0',NULL,1580749288,1,NULL),
(230,'1','Read','view_email_signature_page',2,'email_signature','index','1','0',NULL,1580749288,1,NULL),
(231,'0','Read','view_email_signature_page',3,'email_signature','index','1','0',NULL,1580749288,1,NULL),
(232,'1','Read','edit_email_signature',1,'email_signature','edit','1','0',NULL,1580749288,1,NULL),
(233,'1','Read','edit_email_signature',2,'email_signature','edit','1','0',NULL,1580749288,1,NULL),
(234,'0','Read','edit_email_signature',3,'email_signature','edit','1','0',NULL,1580749288,1,NULL),
(235,'1','Read','view_date_format_page',1,'date_format','index','1','0',NULL,1580749288,1,NULL),
(236,'1','Read','view_date_format_page',2,'date_format','index','1','0',NULL,1580749288,1,NULL),
(237,'0','Read','view_date_format_page',3,'date_format','index','1','0',NULL,1580749288,1,NULL),
(238,'1','Read','edit_date_format',1,'date_format','edit','1','0',NULL,1580749288,1,NULL),
(239,'1','Read','edit_date_format',2,'date_format','edit','1','0',NULL,1580749288,1,NULL),
(240,'0','Read','edit_date_format',3,'date_format','edit','1','0',NULL,1580749288,1,NULL),
(241,'1','Read','view_website_logos_page',1,'website_logos','index','1','0',NULL,1580749288,1,NULL),
(242,'1','Read','view_website_logos_page',2,'website_logos','index','1','0',NULL,1580749288,1,NULL),
(243,'0','Read','view_website_logos_page',3,'website_logos','index','1','0',NULL,1580749288,1,NULL),
(244,'1','Read','edit_website_logos',1,'website_logos','edit','1','0',NULL,1580749288,1,NULL),
(245,'1','Read','edit_website_logos',2,'website_logos','edit','1','0',NULL,1580749288,1,NULL),
(246,'0','Read','edit_website_logos',3,'website_logos','edit','1','0',NULL,1580749288,1,NULL),
(247,'1','Read','view_contact_details_page',1,'contact_details','index','1','0',NULL,1580749288,1,NULL),
(248,'1','Read','view_contact_details_page',2,'contact_details','index','1','0',NULL,1580749288,1,NULL),
(249,'0','Read','view_contact_details_page',3,'contact_details','index','1','0',NULL,1580749288,1,NULL),
(250,'1','Read','edit_contact_details',1,'contact_details','edit','1','0',NULL,1580749288,1,NULL),
(251,'1','Read','edit_contact_details',2,'contact_details','edit','1','0',NULL,1580749288,1,NULL),
(252,'0','Read','edit_contact_details',3,'contact_details','edit','1','0',NULL,1580749288,1,NULL),
(253,'1','Read','view_social_links_page',1,'social_links','index','1','0',NULL,1580749288,1,NULL),
(254,'1','Read','view_social_links_page',2,'social_links','index','1','0',NULL,1580749288,1,NULL),
(255,'0','Read','view_social_links_page',3,'social_links','index','1','0',NULL,1580749288,1,NULL),
(256,'1','Read','edit_social_links',1,'social_links','edit','1','0',NULL,1580749288,1,NULL),
(257,'1','Read','edit_social_links',2,'social_links','edit','1','0',NULL,1580749288,1,NULL),
(258,'0','Read','edit_social_links',3,'social_links','edit','1','0',NULL,1580749288,1,NULL),
(259,'1','Read','view_google_analytics_page',1,'google_analytics','index','1','0',NULL,1580749288,1,NULL),
(260,'0','Read','view_google_analytics_page',2,'google_analytics','index','1','0',NULL,1580749288,1,NULL),
(261,'0','Read','view_google_analytics_page',3,'google_analytics','index','1','0',NULL,1580749288,1,NULL),
(262,'1','Read','view_google_adsense_page',1,'google_adsense','index','1','0',NULL,1580749288,1,NULL),
(263,'0','Read','view_google_adsense_page',2,'google_adsense','index','1','0',NULL,1580749288,1,NULL),
(264,'0','Read','view_google_adsense_page',3,'google_adsense','index','1','0',NULL,1580749288,1,NULL),
(265,'1','Read','view_google_adsense_page',1,'google_adsense','edit','1','0',NULL,1580749288,1,NULL),
(266,'0','Read','view_google_adsense_page',2,'google_adsense','edit','1','0',NULL,1580749288,1,NULL),
(267,'0','Read','view_google_adsense_page',3,'google_adsense','edit','1','0',NULL,1580749288,1,NULL),
(268,'1','Read','view_email_settings_page',1,'email_settings','index','1','0',NULL,1580749288,1,NULL),
(269,'1','Read','view_email_settings_page',2,'email_settings','index','1','0',NULL,1580749288,1,NULL),
(270,'0','Read','view_email_settings_page',3,'email_settings','index','1','0',NULL,1580749288,1,NULL),
(271,'1','Read','view_watermark_settings_page',1,'watermark_settings','index','1','0',NULL,1580749288,1,NULL),
(272,'1','Read','view_watermark_settings_page',2,'watermark_settings','index','1','0',NULL,1580749288,1,NULL),
(273,'0','Read','view_watermark_settings_page',3,'watermark_settings','index','1','0',NULL,1580749288,1,NULL),
(274,'1','Read','edit_watermark_settings_details',1,'watermark_settings','edit','1','0',NULL,1580749288,1,NULL),
(275,'0','Read','edit_watermark_settings_details',2,'watermark_settings','edit','1','0',NULL,1580749288,1,NULL),
(276,'0','Read','edit_watermark_settings_details',3,'watermark_settings','edit','1','0',NULL,1580749288,1,NULL),
(277,'1','Read','edit_languages_details',1,'languages','index','1','0',NULL,1580749288,1,NULL),
(278,'0','Read','edit_languages_details',2,'languages','index','1','0',NULL,1580749288,1,NULL),
(279,'0','Read','edit_languages_details',3,'languages','index','1','0',NULL,1580749288,1,NULL),
(280,'1','Read','view_bookings_listing',1,'bookings','index','1','0',NULL,1580749288,1,NULL),
(281,'1','Read','view_bookings_listing',2,'bookings','index','1','0',NULL,1580749288,1,NULL),
(282,'1','Read','view_bookings_listing',3,'bookings','index','1','0',NULL,1580749288,1,NULL),
(283,'1','Read','view_bookings_details',1,'bookings','view','1','0',NULL,1580749288,1,NULL),
(284,'1','Read','view_bookings_details',2,'bookings','view','1','0',NULL,1580749288,1,NULL),
(285,'1','Read','view_bookings_details',3,'bookings','view','1','0',NULL,1580749288,1,NULL),
(286,'1','Read','delete_booking_details',1,'bookings','delete','1','0',NULL,1580749288,1,NULL),
(287,'1','Read','delete_booking_details',2,'bookings','delete','1','0',NULL,1580749288,1,NULL),
(288,'1','Read','delete_booking_details',3,'bookings','delete','1','0',NULL,1580749288,1,NULL),
(289,'1','Read','view_packages_listing_page',1,'packages','index','1','0',NULL,1580749288,1,NULL),
(290,'1','Read','view_packages_listing_page',2,'packages','index','1','0',NULL,1580749288,1,NULL),
(291,'0','Read','view_packages_listing_page',3,'packages','index','1','0',NULL,1580749288,1,NULL),
(292,'1','Read','add_new_package',1,'packages','add','1','0',NULL,1580749288,1,NULL),
(293,'1','Read','add_new_package',2,'packages','add','1','0',NULL,1580749288,1,NULL),
(294,'0','Read','add_new_package',3,'packages','add','1','0',NULL,1580749288,1,NULL),
(295,'1','Read','edit_package_details',1,'packages','edit','1','0',NULL,1580749288,1,NULL),
(296,'1','Read','edit_package_details',2,'packages','edit','1','0',NULL,1580749288,1,NULL),
(297,'0','Read','edit_package_details',3,'packages','edit','1','0',NULL,1580749288,1,NULL),
(298,'1','Read','delete_package_details',1,'packages','delete','1','0',NULL,1580749288,1,NULL),
(299,'1','Read','delete_package_details',2,'packages','delete','1','0',NULL,1580749288,1,NULL),
(300,'0','Read','delete_package_details',3,'packages','delete','1','0',NULL,1580749288,1,NULL),
(301,'1','Read','view_package_details',1,'packages','view','1','0',NULL,1580749288,1,NULL),
(302,'1','Read','view_package_details',2,'packages','view','1','0',NULL,1580749288,1,NULL),
(303,'0','Read','view_package_details',3,'packages','view','1','0',NULL,1580749288,1,NULL),
(304,'1','Read','view_package_types_listing_page',1,'package_types','index','1','0',NULL,1580749288,1,NULL),
(305,'1','Read','view_package_types_listing_page',2,'package_types','index','1','0',NULL,1580749288,1,NULL),
(306,'0','Read','view_package_types_listing_page',3,'package_types','index','1','0',NULL,1580749288,1,NULL),
(307,'1','Read','add_new_package_type',1,'package_types','add','1','0',NULL,1580749288,1,NULL),
(308,'1','Read','add_new_package_type',2,'package_types','add','1','0',NULL,1580749288,1,NULL),
(309,'0','Read','add_new_package_type',3,'package_types','add','1','0',NULL,1580749288,1,NULL),
(310,'1','Read','edit_package_type_details',1,'package_types','edit','1','0',NULL,1580749288,1,NULL),
(311,'1','Read','edit_package_type_details',2,'package_types','edit','1','0',NULL,1580749288,1,NULL),
(312,'0','Read','edit_package_type_details',3,'package_types','edit','1','0',NULL,1580749288,1,NULL),
(313,'1','Read','delete_package_type_details',1,'package_types','delete','1','0',NULL,1580749288,1,NULL),
(314,'1','Read','delete_package_type_details',2,'package_types','delete','1','0',NULL,1580749288,1,NULL),
(315,'0','Read','delete_package_type_details',3,'package_types','delete','1','0',NULL,1580749288,1,NULL),
(316,'1','Read','edit_languages',1,'languages','edit','1','0',NULL,1580749288,1,NULL),
(317,'1','Read','edit_languages',2,'languages','edit','1','0',NULL,1580749288,1,NULL),
(318,'0','Read','edit_languages',3,'languages','edit','1','0',NULL,1580749288,1,NULL),
(319,'1','Read','edit_email_settings',1,'email_settings','edit','1','0',NULL,1580749288,1,NULL),
(320,'1','Read','edit_email_settings',2,'email_settings','edit','1','0',NULL,1580749288,1,NULL),
(321,'0','Read','edit_email_settings',3,'email_settings','edit','1','0',NULL,1580749288,1,NULL),
(322,'1','Read','view_subscribers_listing_page',1,'subscribers','index','1','0',NULL,1580749288,1,NULL),
(323,'1','Read','view_subscribers_listing_page',2,'subscribers','index','1','0',NULL,1580749288,1,NULL),
(324,'0','Read','view_subscribers_listing_page',3,'subscribers','index','1','0',NULL,1580749288,1,NULL),
(325,'1','Read','edit_subscriber_details',1,'subscribers','edit','1','0',NULL,1580749288,1,NULL),
(326,'1','Read','edit_subscriber_details',2,'subscribers','edit','1','0',NULL,1580749288,1,NULL),
(327,'0','Read','edit_subscriber_details',3,'subscribers','edit','1','0',NULL,1580749288,1,NULL),
(328,'1','Read','view_contact_inquries_listing_page',1,'contact_inquries','index','1','0',NULL,1580749288,1,NULL),
(329,'1','Read','view_contact_inquries_listing_page',2,'contact_inquries','index','1','0',NULL,1580749288,1,NULL),
(330,'0','Read','view_contact_inquries_listing_page',3,'contact_inquries','index','1','0',NULL,1580749288,1,NULL),
(331,'1','Read','view_contact_inquiry_page',1,'contact_inquries','view','1','0',NULL,1580749288,1,NULL),
(332,'1','Read','view_contact_inquiry_page',2,'contact_inquries','view','1','0',NULL,1580749288,1,NULL),
(333,'0','Read','view_contact_inquiry_page',3,'contact_inquries','view','1','0',NULL,1580749288,1,NULL),
(334,'1','Read','view_emails_listing_page',1,'emails','index','1','0',NULL,1580749288,1,NULL),
(335,'1','Read','view_emails_listing_page',2,'emails','index','1','0',NULL,1580749288,1,NULL),
(336,'0','Read','view_emails_listing_page',3,'emails','index','1','0',NULL,1580749288,1,NULL),
(337,'1','Read','view_email_details_page',1,'emails','view','1','0',NULL,1580749288,1,NULL),
(338,'1','Read','view_email_details_page',2,'emails','view','1','0',NULL,1580749288,1,NULL),
(339,'0','Read','view_email_details_page',3,'emails','view','1','0',NULL,1580749288,1,NULL);

/*Table structure for table `reminders` */

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `rem_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `rem_title` varchar(255) NOT NULL,
  `rem_desc` text NOT NULL,
  `rem_date` date NOT NULL,
  `rem_time` time NOT NULL,
  `status` enum('Active','Completed') NOT NULL DEFAULT 'Active',
  `send_to_users` enum('0','1') NOT NULL,
  `send_to_subscribers` enum('0','1') NOT NULL,
  `email_status` enum('0','1') NOT NULL DEFAULT '0',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`rem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Reminders table';

/*Data for the table `reminders` */

/*Table structure for table `system_logs` */

DROP TABLE IF EXISTS `system_logs`;

CREATE TABLE `system_logs` (
  `log_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(10) DEFAULT NULL,
  `operation` varchar(255) DEFAULT NULL,
  `log_type` varchar(255) DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `os` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `timestamp` bigint DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

/*Data for the table `system_logs` */

insert  into `system_logs`(`log_id`,`rand_id`,`operation`,`log_type`,`user_id`,`ip`,`os`,`browser`,`url`,`timestamp`) values 
(1,'6WRVlP','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579951709),
(2,'6gUDLf','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579951743),
(3,'Ncdjtm','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579952207),
(4,'3iuRhf','User can\'t login due to wrong password, user_id( 1)','unauthorised',0,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953256),
(5,'0f4FdB','User can\'t login due to wrong credentials','unauthorised',0,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953270),
(6,'sOHXEZ','User can\'t login due to wrong credentials','unauthorised',0,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953347),
(7,'nadJOT','User can\'t login due to wrong password, user_id( 1)','unauthorised',0,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953356),
(8,'XoIv2O','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953362),
(9,'s7on5M','User can\'t login due to wrong password, user_id( 1)','unauthorised',0,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953438),
(10,'6ojzAW','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953443),
(11,'kRq9PD','User can\'t login due to wrong password, user_id( 1)','unauthorised',0,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953853),
(12,'BrahXZ','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953856),
(13,'pY7mAB','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579953929),
(14,'OKVx8s','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579954006),
(15,'E97p8Z','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579956460),
(16,'JjeQba','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579957303),
(17,'imovOt','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1579958103),
(18,'zVHfAY','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580031199),
(19,'JWzqVY','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580037939),
(20,'8xu9Eg','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580141369),
(21,'jhSJft','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580142957),
(22,'Cl1avx','User can\'t login due to wrong password, user_id( 1)','unauthorised',0,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580763072),
(23,'EYtiAu','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580763078),
(24,'QJlsfV','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580745205),
(25,'04MiLD','Starting migration UP for: Migration_Add_Permissions_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749287),
(26,'1wPTXR','Finished migration UP for: Migration_Add_Permissions_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749288),
(27,'S4wRmp','Initializing permissions.','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749288),
(28,'ycWRe7','Starting migration UP for: Migration_Add_Emails_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749288),
(29,'R0LCfT','Finished migration UP for: Migration_Add_Emails_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749289),
(30,'cJ1a0D','Starting migration UP for: Migration_Add_Subscribers_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749289),
(31,'GN5yYv','Finished migration UP for: Migration_Add_Subscribers_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749289),
(32,'bvSzYw','Starting migration UP for: Migration_Add_About_Us_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749289),
(33,'g7sOTo','Finished migration UP for: Migration_Add_About_Us_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749290),
(34,'yG5Pop','Starting migration UP for: Migration_Add_Terms_And_Conditions_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749291),
(35,'h183FZ','Finished migration UP for: Migration_Add_Terms_And_Conditions_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749291),
(36,'WqBsnO','Starting migration UP for: Migration_Add_Privacy_And_Policy_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749291),
(37,'jHa36u','Finished migration UP for: Migration_Add_Privacy_And_Policy_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749292),
(38,'c52xWb','Starting migration UP for: Migration_Add_Blogs_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749292),
(39,'JE8BOM','Finished migration UP for: Migration_Add_Blogs_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749293),
(40,'7Z5eGM','Starting migration UP for: Migration_Add_Contact_Inquiries_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749294),
(41,'ivfbEM','Finished migration UP for: Migration_Add_Contact_Inquiries_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749294),
(42,'VdJsB6','Starting migration UP for: Migration_Add_Tags_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749294),
(43,'QSEZMR','Finished migration UP for: Migration_Add_Tags_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749295),
(44,'Uy9jmu','Starting migration UP for: Migration_Add_Home_Slider_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749296),
(45,'2tIq0r','Finished migration UP for: Migration_Add_Home_Slider_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749296),
(46,'FnWuOb','Starting migration UP for: Migration_Add_Home_Gallery_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749297),
(47,'i0uTSn','Finished migration UP for: Migration_Add_Home_Gallery_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749297),
(48,'igKlSL','Starting migration UP for: Migration_Add_Testimonial_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749297),
(49,'iwSIxy','Finished migration UP for: Migration_Add_Testimonial_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749299),
(50,'pNbPzM','Starting migration UP for: Migration_Add_Notifications_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749299),
(51,'kD0hHX','Finished migration UP for: Migration_Add_Notifications_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749301),
(52,'2yau7K','Starting migration UP for: Migration_Add_Reminders_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749301),
(53,'MbSV1I','Finished migration UP for: Migration_Add_Reminders_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749302),
(54,'ADvtx1','Starting migration UP for: Migration_Add_Albums_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749303),
(55,'gQirtf','Finished migration UP for: Migration_Add_Albums_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749303),
(56,'7i6nty','Starting migration UP for: Migration_Add_Events_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749303),
(57,'NGB3o8','Finished migration UP for: Migration_Add_Events_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749305),
(58,'zJU5Qn','Starting migration UP for: Migration_Add_Event_Types_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749305),
(59,'52ZdNY','Finished migration UP for: Migration_Add_Event_Types_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749306),
(60,'wSnk9T','Starting migration UP for: Migration_Add_Images_Gallery_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749306),
(61,'wdMAXL','Finished migration UP for: Migration_Add_Images_Gallery_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749306),
(62,'Z2NyLn','Starting migration UP for: Migration_Add_Our_Services_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749307),
(63,'t7bEH8','Finished migration UP for: Migration_Add_Our_Services_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749308),
(64,'aWQhZ8','Starting migration UP for: Migration_Add_Notifications_Users_Trans_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749308),
(65,'MAk5JB','Finished migration UP for: Migration_Add_Notifications_Users_Trans_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749310),
(66,'dCLmoa','Starting migration UP for: Migration_Add_tags_entities_trans_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749310),
(67,'RZ1dlh','Finished migration UP for: Migration_Add_tags_entities_trans_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749311),
(68,'zireID','Starting migration UP for: Migration_Add_Albums_Users_Trans_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749311),
(69,'1VTEmU','Finished migration UP for: Migration_Add_Albums_Users_Trans_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749313),
(70,'P7BjSI','Starting migration UP for: Migration_Add_Videos_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749313),
(71,'SwcuBI','Finished migration UP for: Migration_Add_Videos_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749315),
(72,'gykJpD','Starting migration UP for: Migration_Add_Faq_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749315),
(73,'ZtKIRB','Finished migration UP for: Migration_Add_Faq_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749318),
(74,'Fe8SAY','Starting migration UP for: Migration_Add_Contact_Supports_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749319),
(75,'qNvxuz','Finished migration UP for: Migration_Add_Contact_Supports_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749320),
(76,'Jjmq3f','Starting migration UP for: Migration_Add_Failed_Images_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749320),
(77,'aJDM9s','Finished migration UP for: Migration_Add_Failed_Images_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749322),
(78,'mrYSd2','Starting migration UP for: Migration_Add_Bookings_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749322),
(79,'rXc3V5','Finished migration UP for: Migration_Add_Bookings_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749323),
(80,'527owq','Starting migration UP for: Migration_Add_Package_Types_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749325),
(81,'ZdX6FN','Finished migration UP for: Migration_Add_Package_Types_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749325),
(82,'oFf0HJ','Starting migration UP for: Migration_Add_Packages_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749325),
(83,'rJV76a','Finished migration UP for: Migration_Add_Packages_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1580749327),
(84,'peuJzS','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1580749376),
(85,'OnLDVB','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1581164574),
(86,'RYOeUE','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/api/users/user_login',1581164917),
(87,'FA83nV','Starting migration DOWN for: Migration_Add_tags_entities_trans_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146673),
(88,'1pK5Vh','Finished migration DOWN for: Migration_Add_tags_entities_trans_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146674),
(89,'ia5Ebo','Starting migration DOWN for: Migration_Add_Tags_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146674),
(90,'U6bAWX','Finished migration DOWN for: Migration_Add_Tags_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146675),
(91,'fn7SDb','Starting migration DOWN for: Migration_Add_Contact_Inquiries_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146675),
(92,'A1sOSW','Finished migration DOWN for: Migration_Add_Contact_Inquiries_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146678),
(93,'gtVDCS','Starting migration DOWN for: Migration_Add_Blogs_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146678),
(94,'Jwpr6x','Finished migration DOWN for: Migration_Add_Blogs_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146679),
(95,'mOZkz9','Starting migration DOWN for: Migration_Add_Privacy_And_Policy_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146679),
(96,'sJDKlf','Finished migration DOWN for: Migration_Add_Privacy_And_Policy_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146681),
(97,'KLneTm','Starting migration DOWN for: Migration_Add_Terms_And_Conditions_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146682),
(98,'dYlWLD','Finished migration DOWN for: Migration_Add_Terms_And_Conditions_table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146683),
(99,'3S8xWn','Starting migration DOWN for: Migration_Add_About_Us_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146684),
(100,'tWV1xF','Finished migration DOWN for: Migration_Add_About_Us_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146688),
(101,'kOhDFG','Starting migration DOWN for: Migration_Add_Subscribers_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146689),
(102,'OPsERD','Finished migration DOWN for: Migration_Add_Subscribers_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146693),
(103,'hvKfCH','Starting migration DOWN for: Migration_Add_Emails_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146693),
(104,'rpKl7z','Finished migration DOWN for: Migration_Add_Emails_Table','info',1,'127.0.0.1','Linux','Chrome 79.0.3945.130','en/dashboard/migrate',1581146696),
(105,'E7m8d0','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.87','en/api/users/user_login',1581894382),
(106,'rJH0oK','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.106','en/api/users/user_login',1581942460),
(107,'6XYhpc','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.106','en/api/users/user_login',1582308419),
(108,'hAPQl7','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.106','en/api/users/user_login',1582358038),
(109,'oUv7Q2','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.106','en/api/users/user_login',1582358051),
(110,'54f3Di','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.106','en/api/users/user_login',1582389778),
(111,'Nu0k4T','User can\'t login due to wrong credentials','unauthorised',0,'127.0.0.1','Linux','Chrome 80.0.3987.122','en/api/users/user_login',1583319463),
(112,'Jf786n','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.122','en/api/users/user_login',1583319467),
(113,'gFN2vq','User successfully loggedin, user_id( 1)','login',1,'127.0.0.1','Linux','Chrome 80.0.3987.132','en/api/users/user_login',1583653643);

/*Table structure for table `testimonial` */

DROP TABLE IF EXISTS `testimonial`;

CREATE TABLE `testimonial` (
  `tst_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `testimonial` varchar(255) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_desc` varchar(255) NOT NULL,
  `client_image` varchar(255) NOT NULL,
  `client_s_image` varchar(255) NOT NULL,
  `client_l_image` varchar(255) NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`tst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Testimonial table';

/*Data for the table `testimonial` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` enum('0','Male','Female') DEFAULT '0',
  `oauth_provider` varchar(255) DEFAULT NULL,
  `oauth_id` varchar(255) DEFAULT NULL,
  `user_role_id` bigint DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pin_code` varchar(255) DEFAULT NULL,
  `email_verified` enum('0','1') DEFAULT '0',
  `is_root` enum('0','1') DEFAULT '0',
  `avatar` varchar(255) DEFAULT NULL,
  `language_id` bigint DEFAULT NULL,
  `email_verification_token` varchar(255) DEFAULT NULL,
  `failed_login_attempt_count` int DEFAULT NULL,
  `terms_accepted` enum('0','1') DEFAULT '0',
  `visibility` enum('0','1') DEFAULT '1',
  `deleted` enum('0','1') DEFAULT '0',
  `date_created` bigint DEFAULT NULL,
  `user_created` bigint DEFAULT NULL,
  `date_modified` bigint DEFAULT NULL,
  `user_modified` bigint DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`rand_id`,`first_name`,`last_name`,`email`,`password`,`dob`,`gender`,`oauth_provider`,`oauth_id`,`user_role_id`,`phone`,`address`,`city`,`state`,`pin_code`,`email_verified`,`is_root`,`avatar`,`language_id`,`email_verification_token`,`failed_login_attempt_count`,`terms_accepted`,`visibility`,`deleted`,`date_created`,`user_created`,`date_modified`,`user_modified`) values 
(1,'vrpatel','VR','Patel','admin@admin.com','$2y$10$4yWzM6kPrC59zZIZ.plgx.oiAflG1rYAqxm8gKZc7qis/w431zNyO','1997-05-06','Male',NULL,NULL,1,'+91 9587714590',NULL,NULL,NULL,NULL,'1','1',NULL,1,NULL,NULL,'1','1','0',1560785411,1,NULL,NULL);

/*Table structure for table `videos` */

DROP TABLE IF EXISTS `videos`;

CREATE TABLE `videos` (
  `video_id` bigint NOT NULL AUTO_INCREMENT,
  `rand_id` varchar(255) NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `video_desc` text NOT NULL,
  `video_type` varchar(255) NOT NULL,
  `video_url` varchar(255) NOT NULL,
  `video_thumb` varchar(255) NOT NULL,
  `video_s_thumb` varchar(255) NOT NULL,
  `video_l_thumb` varchar(255) NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `seo_keywords` text NOT NULL,
  `seo_desc` text NOT NULL,
  `visibility` enum('0','1') NOT NULL DEFAULT '1',
  `deleted` enum('0','1') NOT NULL DEFAULT '0' COMMENT 'set true if user is deleted by admin',
  `date_created` bigint NOT NULL,
  `date_modified` bigint NOT NULL,
  `user_created` bigint NOT NULL,
  `user_modified` bigint NOT NULL,
  PRIMARY KEY (`video_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Videos table';

/*Data for the table `videos` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
