/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 8.0.19-0ubuntu0.19.10.3 : Database - thepic
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Data for the table `general_config` */

insert  into `general_config`(`config_id`,`rand_id`,`config_name`,`config_value`,`deleted`,`date_created`,`user_created`,`date_modified`,`user_modified`) values 
(1,'email_signature','email_signature','\r\n                                                            \r\n                                                            \r\n                                                            \r\n                                                            <p style=\"color: rgb(34, 34, 34); line-height: 1.6; padding: 0px;\"><b>Best Regards</b></p><div style=\"\"><font color=\"#222222\" size=\"3\"><b>eStudio360.in</b></font><br></div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">Tonk Road, Jaipur</div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">Rajasthan.&nbsp;302018</div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">+91&nbsp;99834 01389</div><div style=\"color: rgb(34, 34, 34); font-size: 13px;\">+91&nbsp;88241 42381</div>                                                                                                                                                                                                                                                                                                                                                ','0',1560786104,1,1564722194,1),
(2,'date_format','date_format','M d, Y','0',1560786104,1,1562928584,1),
(3,'logo_white','logo_white','','0',1560786104,1,1565621272,1),
(4,'logo_black','logo_black','','0',1560786104,1,1565621273,1),
(5,'fb_url','fb_url','https://www.facebook.com/VRPatel.m2/','0',1560786104,1,1562944117,1),
(6,'insta_url','insta_url','','0',1560786104,1,1562944117,1),
(7,'linkdin_url','linkdin_url','','0',1560786104,1,1562944117,1),
(8,'youtube_url','youtube_url','','0',1560786104,1,1562944117,1),
(9,'twitter_url','twitter_url','','0',1560786104,1,1562944117,1),
(10,'whatsapp_number','whatsapp_number','+91','0',1560786104,1,1562944117,1),
(11,'pinterest_url','pinterest_url','','0',1560786104,1,1562944117,1),
(12,'tumblr_url','tumblr_url','','0',1560786104,1,1562944118,1),
(13,'map_lang','map_lang','','0',1560786104,1,1564677661,1),
(14,'map_lati','map_lati','','0',1560786104,1,1564677661,1),
(15,'analytics_code','analytics_code',NULL,'0',1560786104,1,NULL,NULL),
(16,'adsense_code','adsense_code',NULL,'0',1560786104,1,NULL,NULL),
(17,'seo_title','seo_title','','0',NULL,NULL,1564677661,1),
(18,'seo_keywords','seo_keywords','','0',NULL,NULL,1564677661,1),
(19,'seo_descriptions','seo_descriptions','','0',NULL,NULL,1564677661,1),
(20,'time_format','time_format','h:i A','0',NULL,NULL,1562928584,1),
(21,'favicon_icon','favicon_icon','','0',NULL,NULL,1565621275,1),
(22,'watermark_type','watermark_type','image','0',NULL,NULL,1565623108,1),
(23,'watermark_large_thumn_text','watermark_large_thumn_text','theFilmyGyan.com','0',NULL,NULL,1565623108,1),
(24,'watermark_large_thumb_font_size','watermark_large_thumb_font_size','30','0',NULL,NULL,1565623108,1),
(25,'watermark_large_thumb_font_color','watermark_large_thumb_font_color','#ffffff','0',NULL,NULL,1565623108,1),
(26,'watermark_large_thumb_vertical_align','watermark_large_thumb_vertical_align','middle','0',NULL,NULL,1565623108,1),
(27,'watermark_large_thumb_horizontal_align','watermark_large_thumb_horizontal_align','right','0',NULL,NULL,1565623109,1),
(28,'watermark_small_thumb_text','watermark_small_thumb_text','theFilmyGyan.com','0',NULL,NULL,1565623109,1),
(29,'watermark_small_thumb_font_size','watermark_small_thumb_font_size','25','0',NULL,NULL,1565623109,1),
(30,'watermark_small_thumb_font_color','watermark_small_thumb_font_color','#ffffff','0',NULL,NULL,1565623109,1),
(31,'watermark_small_thumb_vertical_align','watermark_small_thumb_vertical_align','bottom','0',NULL,NULL,1565623109,1),
(32,'watermark_small_thumb_horizontal_align','watermark_small_thumb_horizontal_align','center','0',NULL,NULL,1565623109,1),
(33,'watermark_in_news','watermark_in_news','1','0',NULL,NULL,1564769431,1),
(34,'watermark_in_news_types','watermark_in_news_types','1','0',NULL,NULL,1565419659,1),
(35,'watermark_in_text_editor','watermark_in_text_editor','1','0',NULL,NULL,1562957678,1),
(36,'send_email_to_new_subscription','send_email_to_new_subscription','1','0',NULL,NULL,NULL,NULL),
(37,'send_a_thanks_email_when_someone_contact_us','send_a_thanks_email_when_someone_contact_us','1','0',NULL,NULL,NULL,NULL),
(38,'watermark_small_thumb_image','watermark_small_thumb_image','','0',NULL,NULL,1565623108,1),
(39,'watermark_large_thumb_image','watermark_large_thumb_image','','0',NULL,NULL,1565623108,1),
(40,'timezone','timezone','Asia/Kolkata','0',NULL,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
